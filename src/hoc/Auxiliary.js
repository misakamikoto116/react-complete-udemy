// Digunakan di Component Lain sebagai Pembungkus / tapi sekarang sudah ada React Fragment
// Ini Adalah high Order component
const aux = props => props.children;

export default aux;