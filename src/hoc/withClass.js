import React from 'react'

// Cara 1 Pake Props.. element di App langsung Bungkus aja pake withClass
// Dan Di Export Tanpa withclass(App, styles.app)
// const WithClass = (props) => (
//     <div className={props.classes}>{props.children}</div>
// )

// Cara 2 pake Javascript.. Element di App harus pake Yang Aux / fragment... Setelah itu di export dikasi withClass(App, Styles.app) untuk kasih ClassName Nya
// {...} itu gunanya memasukkan props sebelah return ke props component WrappedComponent.. jadi dia ngeluarin value props.name, props.email.. intinya dia memasukkan sama seperti props lah.. habis dikirm kesini dikirim lagi ke Wrapped Component
const withClass = (WrappedComponent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponent {...props}/>
        </div>
    )

}

export default withClass