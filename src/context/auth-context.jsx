// Chain.. Jadi misal ada Component A, B, C, D kita gk perlu ke B dan C dulu.. kita bisa langsung parsing ke D
// Ini namanya ContextAPI
import React from 'react'

// Bisa juga Gk object, contoh CreateContext('Joko')
// buat isi defaultnya
const AuthContext = React.createContext({
    authenticated: false,
    login: () => {}
})

export default AuthContext