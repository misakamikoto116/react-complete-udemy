// Useeffect untuk lifecycle nya react Hooks
import React, { useEffect, useRef, useContext } from 'react';
import styles from '../../assets/css/Cockpit.module.css';
import AuthContext from '../../context/auth-context'

// Harus C Capital
const Cockpit = (props) => {
    const toggleBtnRef = useRef(null);
    // Nama Const Nya sembaran
    const authContext = useContext(AuthContext)
    console.log(authContext);


    // Muncul ketika di mount dan di update (Gabungan Did Mount dan didupdate)
    // Kosongkan array jika hanya ingin componentDidMount
    // Jika array diisi jadi [props.persons] :
    // 1. alert muncul hanya ketika props.persons diubah, bukan saat state apapun yang berubah
    // 2. Alert juga muncul ketika kita render props.persons pertama kali
    // Return dibawah ini sama aja kaya willunMount() 
    useEffect(() => {
        console.log('[cockpit.jsx] useEffect');
        // Http Request
        // dalam 1 detik dia akan melakukan click by Reference dibawah
        const timer = setTimeout(() => {
            toggleBtnRef.current.click()
            // alert('Saved data to cloud!');
        }, 1000)

        return () => {
            clearTimeout(timer);
            console.log('[cockpit.jsx] cleanup work in useEffect');
        }
    }, [])

    // Bisa pake Banyak use effect untuk banyak props dan state
    // jadi sebelum dia melakukan mount ke 2, dia di cleanup dulu... anggapannya kaya re-render / hapus render (Unmount) lalu buat render lagi, dan ini selalu terjadi ketika kita toggle persons juga
    useEffect(() => {
        console.log('[cockpit.jsx] 2nd useEffect')

        return () => {
            console.log('[cockpit.jsx] 2nd cleanup work in useEffect')
        }
    })

    const classes = [];
    let buttonClass = '';

    if (props.showPersons) {
        // mengubah isi variable jadi class red di App.module.css
        buttonClass = styles.red;
    }

    if (props.personsLength <= 2) {
      classes.push(styles.red); // classes = ['red'] => pake class red
    }

    if (props.personsLength <= 1) {
      classes.push(styles.bold) // classes = ['red', 'bold'] => pake keduanya
    }

    return (
        <div className={styles.Cockpit}>
            {/* Pake join biar jadi "red bold" */}
            <p className={classes.join(' ')}>{props.title}</p>
            <button
                ref={toggleBtnRef}
                className={buttonClass}
                onClick={ props.clicked }>
                {props.showPersons ? 'Hide Persons' : 'Show Persons'}
            </button>
            <button onClick={authContext.login}>{ authContext.authenticated ? 'Logout' : 'Login' }</button>
        </div>
    )
}

export default React.memo(Cockpit);