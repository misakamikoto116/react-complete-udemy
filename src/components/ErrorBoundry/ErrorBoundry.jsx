import React, { Component } from "react";

class ErrorBoundry extends Component {
    state = {
        hasError: false,
        errorMessage: ''
    }

    componentDidCatch = (error, info) => {
        this.setState({
            hasError: true,
            errorMessage: error 
        })
    }

    render() {
        let Message = null;
        if (this.state.hasError) {
            Message = <h1>{this.state.hasError}</h1>
        } else {
            Message = this.props.children
        }

        return Message;
    }
}

export default ErrorBoundry;