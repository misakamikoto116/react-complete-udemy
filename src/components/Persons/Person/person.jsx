import React, {Component} from 'react';
import styles from '../../../assets/css/Person.module.css'
import Aux from '../../../hoc/Auxiliary'
import WithClass from '../../../hoc/withClass'
import PropTypes from 'prop-types'
import AuthContext from '../../../context/auth-context'

class Person extends Component {
    constructor(props) {
        super(props);
        this.inputElementRef = React.createRef();
    }

    // Ini Default dari Class
    static contextType = AuthContext;
   
    componentDidMount() {
        // Bikin dia auto focus ke paling bawah
        // Cara 1 / Lama
        // this.inputElement.focus()
        
        // Cara 2
        this.inputElementRef.current.focus();
        // Emang This.Context  Automatis
        console.log(this.context.authenticated)
    }

   render() {
        console.log('[person.jsx] rendering...')
        // Test Jika di errorkan maka akan catch ke ErrorBoundy
        // const rand = Math.random();
        // if (rand > 0.7) {
        //     throw new Error('something went wrong!');
        // }

        return (
            // <div className={styles.Person}>
            // <> = Fragment
            // Consumer = Semacam GET.. dia bakal ngambil authenticate dari auth-context jika true maka jadi sekian jika false maka sekian 
            <Aux>
                <p>{this.context.authenticated ? 'Authenticate!' : 'Please Login!'}</p>
                <p>I'm {this.props.name} and I am {this.props.age} years old!</p>
                <p>{this.props.children}</p>
                <input type="text"
                       // ref={(inputEl) => {this.inputElement = inputEl}} 
                       ref={this.inputElementRef}
                       onChange={this.props.changedMe} 
                       value={this.props.name} />
                <button onClick={this.props.deleteBtn}>Delete</button>
            </Aux>
        )
   } 
}

// .propTypes adalah property bawaan kalau PropTypes baru dari yang atas
// Jika tidak sesuai nnti ada error di console log
Person.propTypes = {
    deleteBtn: PropTypes.func,
    changedMe: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number
}

// Jika pake radium => Radium(person)
export default WithClass(Person, styles.Person);