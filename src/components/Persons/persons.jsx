import React, {PureComponent} from 'react';
import Person from './Person/person'
import ErrorBoundry from '../ErrorBoundry/ErrorBoundry'

// Gunakan PureComponent Jika ingin Melakukan Pengecekan seperti shouldComponentUpdate tapi untuk ke semua props, jadi lebih hemat menulis kode
class Persons extends PureComponent {
    //jarang dipake  
    // static getDerivedStateFromProps(props, state) {
    //    console.log('[persons.jsx] getDerivedStateFromProps'); 
    // }

    // gunanya buat mutate props, tidak digunakan lagi baik pake construct
    // componentWillReceiveProps(props) {
    //     console.log('[persons.jsx] componentWillReceiveProps', props);
    // }

    // Jika props nya gk diubah maka dia gk perlu render ulang / update.. tetapi jika ada perubahan, maka dia akan melakukan perubahan => dilempar ke didupdate
    // kalau mau pakai ini gunakan Component.. tapi jika ingin mengecek semua props maka gunakan aja PureComponent
    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('[persons.jsx] shouldComponentUpdate')
    //     if (
    //         nextProps.persons !== this.props.Person ||
    //         nextProps.clicked !== this.props.clicked ||
    //         nextProps.clicked !== this.props.clicked) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // Mengirim Data ke Compoennt Did Update
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('[persons.jsx] getSnapshotBeforeUpdate')
        return { message: 'Snapshot!' }
    }

    // Jika kita butuh ini, maka kita salah menggunakan react
    // componentWillUpdate() {

    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('[persons.jsx] componentDidUpdate')
        console.log(snapshot)
    }

    // Jika component ini dihapus maka akan muncul console log (Contoh jika compoenent ini disembunyikan atau tidak dipakai, karena state showPersons adalah false)
    // Ini akan muncul ketika kita click "hide Persons"
    componentWillUnmount() {
        console.log('[persons.jsx] CompoenentWillUnmount')
    }

    render() {
        console.log('[persons.jsx] rendering...')
        return this.props.persons.map((person, index) => {
            // error Boundry di wrap ke person aja (anggapannya kaya middleware), yang butuh key dia
            return <ErrorBoundry key={person.id}>
                        <Person 
                        name={person.name} 
                        age={person.age}
                        deleteBtn={ () => this.props.clicked(index)}
                        changedMe={ (e) => this.props.changed(e, person.id)}
                        // Kita gk pake ini lagi karena dia dari App langsung ke Person tanpa harus kesini dulu
                        // isAuthenticate={this.props.isAuthenticate}
                        />
                    </ErrorBoundry> 
        })
    }
}

export default Persons;