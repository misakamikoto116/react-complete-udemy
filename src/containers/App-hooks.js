// Jangan Lupa pake useState
import React, { useState } from 'react';
import './App.css';
import Person from './Person/person.jsx';

const App = props => {

  // Membuat State
  const [personsState, setPersons] = useState({
    persons: [
      { name : 'Joel', age: 19 },
      { name : 'Alya', age: 13 },
      { name : 'Random Person', age: Math.floor(Math.random() * 30) }
    ]
  })

  // Membuat State 2 / Bisa juga kok di gabung sama diatas, tapi setiap update harus ambil old nya, kalau gini gk perlu
  const [otherState, setOtherState] = useState('This is another State')

  console.log(personsState, otherState);

  const switchNameHandler = () => {
    // Cara Set nya dengan param ke dua
    setPersons({
      persons: [
        { name : 'Julian effendi', age: 20 },
        { name : 'Azizah Alyani', age: 14 },
        { name : 'Random Person', age: Math.floor(Math.random() * 30) }
      ],
      copiedOtherState: otherState
    });
  }

  return (
    <div className="App">
      <h1>I'm a react developer</h1>
      <p>This is really working</p>
      <button onClick={ switchNameHandler }>Switch Name</button>
      <Person name={personsState.persons[0].name} age={personsState.persons[0].age}/>
      <Person name={personsState.persons[1].name} age={personsState.persons[1].age}/>
      <Person name={personsState.persons[2].name} age={personsState.persons[2].age}>Hobinya makan ter00s</Person>
      <p>{ otherState }</p>
    </div>
  );

}

export default App;
