import React, {Component} from 'react';
// namanya harus .module.css untuk pake module-css
import styles from './../assets/css/App.module.css';
// jika pake Radium / Inline css
// import Radium, { StyleRoot } from 'radium';
import Persons from '../components/Persons/persons';
import Cockpit from '../components/Cockpit/cockpit';
import Aux from '../hoc/Auxiliary'
import withClass from '../hoc/withClass'
import AuthContext from '../context/auth-context'

class App extends Component {
  //Gk perlu pake ini kalau udh ada state = {}, buat inisial ini bisa dipake, kaya this.appurl
  constructor(props) {
    super(props);
    console.log('[App.js] Constructor')
  }

  state = {
    persons: [
      { id: 1, name : 'Joel', age: 19 },
      { id: 2, name : 'Alya', age: 13 },
      { id: 3, name : 'Random Person', age: Math.floor(Math.random() * 30) }
    ],
    showPersons : false,
    showCockpit: true,
    counter: 0,
    authenticate: false,
  };

  // Update State ke props, Kalau ini sepertinya pake construct udh cukup
  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] getDerivedStateFromProps', props);
    return state;
  }

  // Bisa jadi di hapus di masa depan
  // componentWillMount() {
  //   console.log('[App.js] CompoentWillmount');
  // }

  componentDidMount() {
    console.log('[App.js] ComponentDidMount')
  }

  // harus di return, jika false dia gk bisa pencet show person, jika true dia bisa, ini bagus buat validation, jika true maka dia mengijinkan update state / props
  shouldComponentUpdate(nextProps, nextState) {
    console.log('[App.js] shouldComponentUpdate')
    return true;
  }

  componentDidUpdate() {
    console.log('[App.js] componentDidUpdate')
  }

  switchNameHandler = (NewName) => {
    // Seriously Don't do this
    // this.state.persons[0].name = 'Julian Effendi';
    this.setState({
      persons: [
        { id: 1, name : 'Julian effendi', age: 20 },
        { id: 2, name : 'Azizah Alyani', age: 14 },
        { id: 3, name : NewName, age: Math.floor(Math.random() * 30) }
      ]
    });
  }

  nameChangedHandler = (e, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    });

    // Copy array Yang lama ubah yang baru
    //Cara < ES6
    // const person = Object.assign({}, this.state.person[personIndex]);

    const person = {
      ...this.state.persons[personIndex]
    }

    // Ubah State Person.name / yang copyan yang sudah difilter diatas
    person.name = e.target.value;
    
    const persons = [...this.state.persons];
    persons[personIndex] = person

    // Baru ubah statenya berdasarkan yang udh kita copy tadi
    // Jika kita mengambil nilai state sebelumnya maka kita perlu melakukan ini, Jadi jangan langsung asal counter : this.state.counter + 1, tapi kalo true false gpp biasa
    // Jika tidak maka biasa saja setStatenya
    this.setState((prevState, props) => {
      return {
        persons: persons,
        counter: prevState.counter + 1
      }
    })
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons
    this.setState({
      showPersons : !doesShow,
    });
  }

  deletePersonHandler = (index) => {
    // Mengopy Yang lama lalu mutate yang baru

    // Cara JS <ES6
    // const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({persons: persons});
  }

  toggleCockpitHandler = () => {
    const doesShow = this.state.showCockpit;
    this.setState({
      showCockpit: !doesShow
    });
  }

  loginHandler = () => {
    const doesLogin = this.state.authenticate;
    this.setState({ authenticate: !doesLogin})
  }
  

  render() {
    console.log('[App.js] Render')
    // Inline Css / tidak global di branch inline-css

    let PersonsForm = null

    // Bisa if diisni bisa juga ternary.. tapi recommended ini, Jika gk ada buttonnya, boleh hapus aja div nya
    if (this.state.showPersons) {
      PersonsForm = 
        <div>
          <button onClick={() => this.switchNameHandler('This is America!!')}>Refresh Persons</button>
          <Persons 
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangedHandler}
          />
      </div>
    }

    return (
        <Aux>
          <h1>I'm a react developer</h1>
          <button onClick={this.toggleCockpitHandler}>Toggle Cockpit</button>
          {/* Provider itu semacam Kaya SET */}
          <AuthContext.Provider value={{
            authenticated: this.state.authenticate,
            login: this.loginHandler
          }}>
            {/* Sama seperti showCockpit : null Pake && */}
            { this.state.showCockpit &&
              <Cockpit 
                title={this.props.title}
                showPersons={ this.state.showPersons }
                personsLength={ this.state.persons.length }
                clicked={ this.togglePersonsHandler }
                />   
                // Dah pake context gk perlu ini, Propsnya disimpan di context
                // login={this.loginHandler}
                // isAuthenticate={this.state.authenticate}
            }
            { PersonsForm }
          </AuthContext.Provider>
        </Aux>
    );
  }
}

export default withClass(App, styles.App);